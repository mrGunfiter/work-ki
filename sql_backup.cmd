rem ��������� ������� ����
set DD=%DATE:~0,2%
set MM=%DATE:~3,2%
set YY=%DATE:~6,4%
rem �������� ��� �������� ������� ����� SQL-���
set Dir1=\\192.168.1.62\d$\BACKUP\ZUP_Impex\
set Dir2=\\192.168.1.62\d$\BACKUP\ZUP_Active\
set Dir3=\\192.168.1.62\d$\BACKUP\trade_msk\
set Dir4=\\192.168.1.62\d$\BACKUP\buh_msk\
set Dir5=\\192.168.1.62\d$\BACKUP\BU_8.2\
set Dir6=\\192.168.1.62\d$\BACKUP\Active\
set Dir7=\\192.168.1.62\d$\BACKUP\Impex\
rem ������� ��� ������
set BackupDir=D:\Backup\SQL_Backup\
rem �� ������� ���� ������ ������� ����� SQL �����
set DayLeft=8
rem ����� ������ SQL �����
set fileMask=*.bak
rem ����-�������� 
set SQL_fileLog=D:\Backup\SQL_Backup.log

call :write2log "START" %SQL_fileLog%
call :CheckOneDir %BackupDir% %fileMask% %DayLeft%
call :CheckOneDir D:\Backup\elastix\ elastixbackup-*-ab.tar 5
call :OneDir %Dir1% %fileMask% %BackupDir%
call :OneDir %Dir2% %fileMask% %BackupDir%
call :OneDir %Dir3% %fileMask% %BackupDir%
call :OneDir %Dir4% %fileMask% %BackupDir%
call :OneDir %Dir5% %fileMask% %BackupDir%
call :OneDir %Dir6% %fileMask% %BackupDir%
call :OneDir %Dir7% %fileMask% %BackupDir%
call :write2log "FINISH" %SQL_fileLog%

exit

rem ������� ����������� ������ �� ������ ��������
:OneDir
xcopy %1%2 %3 /f /z /r /h /y /d:%MM%-%DD%-%YY% >>%SQL_fileLog%
exit /b

rem ������� �������� ������ �� ������ ��������
:CheckOneDir
d:
cd %1
call :write2log "Delete %2 older than %3 days from %1:" %SQL_fileLog%
forfiles /P %1 /M %2 /D -%3 /C "cmd /c if not exist @file\ echo @file >> %SQL_fileLog%"
forfiles /P %1 /M %2 /D -%3 /C "cmd /c if not exist @file\ del /F /Q @file"
call :write2log "=================================" %SQL_fileLog%
exit /b

rem ������� ������ � log
:write2Log message
  echo 
  echo %date% %time% %~1 >> %2
exit /b
