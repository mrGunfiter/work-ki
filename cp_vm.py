#!/usr/bin/env python3.6

#
# 7-9.11.2017 (c) Cherevko S.V.
# в честь 100-летия революции :)
#
# копируем файлы полных бэкапов от виртуальных машин
# на резервный сервер
#
#

import os
import shutil
import filecmp
import logging
import traceback
from datetime import datetime, date, time

# файлы старше удалять на приемнике, младше - копировать
iDays = 5

# маска файлов
cMask = '.vbk'

# лог-файл
cLog = '/var/www/backup/cp_vm-py.log'

# исходные папки
cFromDirs = ['/mnt/backland/VM_Back/SQL01_DUBLIN/',
             '/mnt/backland/VM_Back/1C_Main_SYDNEY/',
             '/mnt/backland/VM_Back/TS01_TULUZA/']

# результирующая папка
cToDir = '/var/www/backup/vm/'

# логгер
logger = logging.getLogger('CP_VM.PY')
logger.setLevel(logging.INFO)
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# для консоли
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(formatter)

# для вывода в лог-файл
log = logging.FileHandler(cLog)
log.setLevel(logging.INFO)
log.setFormatter(formatter)

# Инициализируем
logger.addHandler(console)
logger.addHandler(log)
logger.info('Job START')


# возраст файла в днях
def fileAge(cFullFileName):
    try: 
       return int((datetime.today().timestamp() - os.path.getmtime(cFullFileName))/86400)
    except FileNotFoundError:
       return -1
# def fileAge


# в папке cDir удаляем файлы старше iDays
def clearOld(cDir, iDays, cMask):
    cFiles = os.listdir(cDir)
    for i in range(len(cFiles)):
        if cFiles[i].find(cMask) > -1:
            iFileAge = fileAge(os.path.join(cDir, cFiles[i]))
            # print('File age -', iFileAge, iFileAge > iDays)
            if iFileAge > iDays:
                logger.info('Delete file older than ' + str(iDays) + ' days: ' + cFiles[i] + ' (' + str(iFileAge) + ' days)')
                os.remove(os.path.join(cDir, cFiles[i]))
# def clearOld()


# сравнение файлов
def cmpFiles(inFile, outFile):
    eq = filecmp.cmp(inFile, outFile) # сравнить файлы
    if eq: 
        return True
    else:
        return False
#def cmpFiles(inFile, outFile)


# Проверка наличия новых файлов
def checkNew():
    newFilesCount = 0
    for i in range(len(cFromDirs)):
        cFiles = []
        cFiles = os.listdir(cFromDirs[i])
        for j in range(len(cFiles)):
            if cFiles[j].find(cMask) > -1:
                iFileAge = fileAge(os.path.join(cFromDirs[i], cFiles[j]))
                if iFileAge <= iDays:
                    newFilesCount += 1
    logger.info('New files found - ' + str(newFilesCount) + ', old files can be removed...')
    return newFilesCount
# def checkNew(cFiles)


# проверка необходимости копирования
def needCopy(cFile):
    if os.path.exists(os.path.join(cToDir, cFile)): # если файл существует
        if cmpFiles(os.path.join(cFromDirs[i], cFile),  os.path.join(cToDir, cFile)):
            return False
        else:
            return True
    else:
        return True
#def needCopy()

#
# точка входа
#

try:
    # очищаем место - удаляем старые файлы
    if checkNew() > 0:
        clearOld(cToDir, iDays, cMask)
    # просмотр каталогов
    for i in range(len(cFromDirs)):
        logger.info('Check folders: ' + cFromDirs[i])
        cFiles = []
        cFiles = os.listdir(cFromDirs[i])
        # копируем при необходимости
        for j in range(len(cFiles)):
            if cFiles[j].find(cMask) > -1:
                iFileAge = fileAge(os.path.join(cFromDirs[i], cFiles[j]))
                if iFileAge <= iDays:
                    logger.info(cFiles[j] + ' - younger than ' + str(iDays) + ' days, will be copied' )
                    if needCopy(cFiles[j]):
                        shutil.copy2(os.path.join(cFromDirs[i], cFiles[j]), cToDir)
                        if os.path.exists(os.path.join(cToDir, cFiles[j])):
                            logger.info('OK')
                        else:
                            logger.error('File is NOT copied !')
                    else:
                        logger.info('SKIPPED  - file exists and equal to the original' )
except FileNotFoundError:
    logger.error('Dir or file NOT FOUND! check mounted volumes!')
except Exception:
    logger.error(traceback.format_exc())
logger.info('Job FINISH')
