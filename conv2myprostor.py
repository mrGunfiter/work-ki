"""
24-26.10.2017
(с) Черевко С.В.

Конвертер CSV файлов от kbpoptom.ru
для загрузки на my-prostor.ru

сваяно на python'е 3.6.2 в целях ознакомления с оным ;)

"""


import csv
import os
import time


# маска для исходных файлов
in_mask='export'
# маска для результирующих файлов
out_mask='my-prostor'
# добавлять к имени файла штамп даты-времени
bStampDate = True
# путь где искать исходные файлы
work_dir = 'd:\\_iShop\\goods\\'
# путь для результирующих файлов
out_dir = 'd:\\_iShop\\4import\\'
# один результирующий файл
bAllIn = False

kef1 = 0.4
kef2 = 0.8

# проверить является ли строка числом
def is_digit(string):
    if string.isdigit():
       return True
    else:
        try:
            float(string)
            return True
        except ValueError:
            return False
# def is_digit(string)

# инициализация словаря для результирующего CSV-файла
def exportline_fill(line, aCategory):
        exportline = dict()
        if (len(aCategory) == 1):
            exportline['Корневая'] = aCategory[0]
            exportline['Подкатегория 1'] = ''
            exportline['Подкатегория 2'] = ''
            exportline['Подкатегория 3'] = ''
        elif (len(aCategory) == 2):
            exportline['Корневая'] = aCategory[0]
            exportline['Подкатегория 1'] = aCategory[1]
            exportline['Подкатегория 2'] = ''
            exportline['Подкатегория 3'] = ''
        elif (len(aCategory) == 3):
            exportline['Корневая'] = aCategory[0]
            exportline['Подкатегория 1'] = aCategory[1]
            exportline['Подкатегория 2'] = aCategory[2]
            exportline['Подкатегория 3'] = ''
        elif (len(aCategory) == 4):
            exportline['Корневая'] = aCategory[0]
            exportline['Подкатегория 1'] = aCategory[1]
            exportline['Подкатегория 2'] = aCategory[2]
            exportline['Подкатегория 3'] = aCategory[3]                              
        exportline['Артикул'] = line['Артикул'].strip()
        exportline['Название товара'] = line['Наименование'].strip()
        exportline['Краткое описание'] = line['Описание'].strip()
        exportline['Полное описание'] = ''
        exportline['Изображения'] = line['Изображение'].strip()
        exportline['Цена закупки'] = ''
        exportline['Параметр: Ткань'] = ''
        exportline['Свойство: Цвет'] = ''
        exportline['Цена продажи'] = ''
        exportline['Старая цена'] = ''                 
        return exportline
# def exportline_fill(line)

# сообсно сама конвертация  
def csv_work(file_obj, fileOUT, aCategory):   
    reader = csv.DictReader(file_obj, delimiter = ';')
    aData = []
    for line in reader:
        exportline = exportline_fill(line, aCategory)
        tmpSize = line['РАЗМЕР'].strip()
        if len(tmpSize) == 0: continue
        delimiterpos = tmpSize.find(';')
        while delimiterpos != -1:
            Size = tmpSize[0:delimiterpos]
            exportline['Свойство: Размер'] = str(Size)
            price_zak = line[Size].replace('руб.', '')
            price_zak = int(price_zak.replace(' ', ''))
            exportline['Цена закупки'] = price_zak
            price_now = int((price_zak + price_zak * kef1)/10)*10
            if kef2 == 0:
                price_old = ''
            else:
                price_old = int((price_zak + price_zak * kef2)/10)*10    
            exportline['Цена продажи'] = price_now
            exportline['Старая цена'] = price_old         
            tmpSize = tmpSize[(delimiterpos + 2):(len(tmpSize))]
            delimiterpos = tmpSize.find(';')
            aData.append(exportline)
            exportline = exportline_fill(line, aCategory)
        else:
            Size = tmpSize
            exportline['Свойство: Размер'] = Size;
            price_zak = line[Size].replace('руб.', '')
            price_zak = int(price_zak.replace(' ', '')) 
            exportline['Цена закупки'] = price_zak
            price_now = int((price_zak + price_zak * kef1)/10)*10
            if kef2 == 0:
                price_old = ''
            else:
                price_old = int((price_zak + price_zak * kef2)/10)*10    
            exportline['Цена продажи'] = price_now
            exportline['Старая цена'] = price_old           
            aData.append(exportline)
    # пишем в файл
    if not os.path.exists(fileOUT):
        with open(fileOUT, 'w', encoding='cp1251', newline = '') as file:
            columns = ['Корневая', 'Подкатегория 1', 'Подкатегория 2', 'Подкатегория 3','Артикул','Название товара','Краткое описание','Полное описание','Изображения','Свойство: Размер','Цена закупки','Параметр: Ткань','Свойство: Цвет','Цена продажи','Старая цена']
            writer = csv.DictWriter(file, fieldnames=columns, delimiter=';')
            writer.writeheader()
            writer.writerows(aData)
    else:
        with open(fileOUT, 'a', encoding='cp1251', newline = '') as file:
            columns = ['Корневая', 'Подкатегория 1', 'Подкатегория 2', 'Подкатегория 3','Артикул','Название товара','Краткое описание','Полное описание','Изображения','Свойство: Размер','Цена закупки','Параметр: Ткань','Свойство: Цвет','Цена продажи','Старая цена']
            writer = csv.DictWriter(file, fieldnames=columns, delimiter=';')
            writer.writerows(aData)         
# def csv_work

def DateTime2str(bFull):
    if bFull:
        return time.strftime('%d%m%Y-%H%M%S')
    else:
        return time.strftime('%d%m%Y')
# def DateTime2str 

#
# точка входа
#
if bAllIn:
    fileOUT = out_dir + out_mask + DateTime2str() + '.csv'
for top, dirs, files in os.walk(work_dir):
    for nm in files:
        # Поиск нужных файлов    
        if ((nm.find(in_mask) > -1) and (nm.find('.csv') > -1)):
            # файл найден, из его пути формируем категории    
            cNameCategories = top.replace(work_dir, '')       
            aCategory = []
            delimiterpos = cNameCategories.find('\\')
            while (delimiterpos != -1):
                aCategory.append(cNameCategories[0:delimiterpos])                               
                cNameCategories = cNameCategories[(delimiterpos + 1):(len(cNameCategories))]
                delimiterpos = cNameCategories.find('\\')
            else:
                aCategory.append(cNameCategories)
            # имя результирующего файла
            if bStampDate:
                cDateStamp = DateTime2str(True)
            else:
                cDateStamp = ''
            if (not bAllIn):
                fileOUT = out_dir + out_mask + '_' + cDateStamp + '_' + aCategory[len(aCategory) - 1] + '.csv' 
            print()
            print('Обработка ', os.path.join(top, nm))
            print('Категория',  aCategory[len(aCategory) - 1])
            fileIN = os.path.join(top, nm)
            # сообсно сама конвертация    
            with open(fileIN) as f_obj:
                kef1 = 0.4
                kef2 = 0.8
                strIN = ''
                strIN = input('Введите наценку в % (т.е. 40 для 40%, ENTER для 40%)): ')
                if is_digit(strIN):
                    kef1 = int(strIN)/100
                else:
                    kef1 = 0.4    
                    print('Введено не число, наценка - 40%')
                strIN = ''        
                strIN = input('Введите наценку для СТАРОЙ цены в % (т.е. 80 для 80%, ENTER для (80%)): ')
                if is_digit(strIN):
                    kef2 = int(strIN)/100
                else:
                    kef2 = 0.8
                    print('Введено не число, наценка для СТАРОЙ цены - 80%')
                csv_work(f_obj, fileOUT, aCategory)
            NewName = fileIN.replace('export', DateTime2str(True) + '_done')
            os.replace(fileIN, NewName)       
            print('Данные загружены в ', fileOUT)
            
print()
print('ГОТОВО!')
input('ENTER  для завершения')
# зе енд