#! /bin/bash
#(c) Cherevko S.V. 11.07.2018
# Автоматическое резервное копирование файловых ресурсов
#
flog=/var/log/backup-log.txt
dtFormat="+%Y-%m-%d %H:%M:%S"
log() {
    echo $(date "${dtFormat}")" $1" >>$flog
}

removeOld(){
    folder=$1
    days=$2
    patt=$3
    lst=/tmp/cleanup-backup-list.txt
    find -P "${folder}" -maxdepth 4 -mtime +${days} -iname "${patt}" -delete -print >${lst}
    cnt=$(grep -c "" ${lst} )
    log "OK: ${cnt} files like '${patt}' removed from '${folder}'"
}


copyNEW(){
    if [ -f /mnt/dexter/disk_d/dexter_drive_d.txt ]; then
       log "/mnt/dexter/disk_d/ already mounted"
    else
        mount -t cifs //192.168.1.2/d$ /mnt/dexter/disk_d -o credentials=/home/shareloginpass1
    fi
#    mount -t nfs 192.168.1.78:/mnt/share /mnt/share

    if [ -f /mnt/dexter/disk_d/dexter_drive_d.txt ]; then
        log "START copy from DEXTER"
#        ls -l -R /mnt/dexter/disk_d/Shares
#        ls -l -R /mnt/dexter/disk_d/Switch
#        ls -l -R /mnt/dexter/disk_d/Veda
#        ls -l -R /mnt/dexter/soft
        cp -u -r /mnt/dexter/disk_d/Shares /backup/dexter/
        cp -u -r /mnt/dexter/disk_d/Switch /backup/dexter/
        cp -u -r /mnt/dexter/disk_d/Veda /backup/dexter/
        log "END copy from DEXTER"
    else
        log "ERROR CAN'T START copy from DEXTER - DRIVE NOT MOUNT!!!!"
    fi

    if [ -f /mnt/dexter/soft/dirsoft.txt ]; then
       log "/mnt/dexter/soft/ already mounted"
    else
        mount -t cifs //192.168.1.2/e$/soft /mnt/dexter/soft -o credentials=/home/shareloginpass1
    fi
    if [ -f /mnt/dexter/disk_d/dexter_drive_d.txt ]; then
        log "START copy from DEXTER/Soft"
        cp -u -r /mnt/dexter/soft /backup/dexter/
        log "END copy from DEXTER/Soft"
    else
        log "ERROR CAN'T START copy from DEXTER/Soft - DRIVE NOT MOUNT!!!!"
    fi

    if [ -f /mnt/rome/rome.bkp ]; then
       log "/mnt/rome/ already mounted"
    else
        # mount -t cifs //192.168.1.80/d$/Production /mnt/rome -o credentials=/home/shareloginpass1
        mount -t cifs //192.168.1.7/e$/Backup/fBackup/rome /mnt/rome -o credentials=/home/shareloginpass1
    fi
    if [ -f /mnt/rome/rome.bkp ]; then
        log "START copy from ROME"
#        ls -l -R /mnt/rome
        cp -u -r /mnt/rome /backup/rome/
        log "END copy from ROME"
    else
        log "ERROR CAN'T START copy from ROME - DRIVE NOT MOUNT!!!!"
    fi

    if [ -f /mnt/horton/horton_drive_d.txt ]; then
       log "/mnt/horton/ already mounted"
    else
        mount -t cifs //192.168.1.111/d$ /mnt/horton -o credentials=/home/shareloginpass1
    fi
    if [ -f /mnt/horton/horton_drive_d.txt ]; then
        log "START copy from HORTON"
#        ls -l -R /mnt/horton/Userdata
        cp -u -r  /mnt/horton/Userdata /backup/horton/
        log "END copy from HORTON"
    else
        log "ERROR CAN'T START copy from HORTON - DRIVE NOT MOUNT!!!!"
    fi

    log "END BIG copy process :)"
    umount /mnt/dexter/disk_d
    umount /mnt/dexter/soft
    umount /mnt/horton
    umount /mnt/rome
}

removeOld '/backup/elastix' "7" 'elastixbackup-*-ab.tar'
log "Backup files from servers started"
copyNEW
log "Backup complete"
exit 0
